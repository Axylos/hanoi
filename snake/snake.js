(function(root) {
  "use strict";
  var Snake = root.Snake = (root.Snake || {});
  
  var Snake = Snake.Snake = function() {
    this.board = new Board();
    this.coords = [
            new Coord(2, 4),
            new Coord(3, 4), 
            new Coord(4, 4), 
            new Coord(5, 4) 
          ];
          
    this.dir = "E";
  }
  
  var Board = Snake.Board = function() {
    
  }
  
  var Coord = Snake.Coord = function(x,y) {
    this.x = x;
    this.y = y;
  }
  
  Coord.prototype.add = function(other_coord) {
    return new Coord(
      this.x + other_coord.x,
      this.y + other_coord.y
    );
  }
  
  Snake.prototype.move = function() {
    var head = this.coords[this.coords.length - 1];
    this.coords.push(head.add(Snake.DIRDELTAS[this.dir]));
    this.board[this.coords.shift()];
  }
  
  Board.SIZE = 8;
  
  
  Board.prototype.render = function(snake) {
    var board = this;
    
    snake.coords.forEach(function(coord) {
      board[coord.x + Board.SIZE * coord.y] = "S";
    })
    
    var render = ""
    
    for (var i = 0; i < Math.pow(Board.SIZE, 2); i++) {
      render += board[i] || ".";
      if (i % 8 === 7 ) {
        render += "\n";
      }
    }
    
    return render;
  }
  
  Snake.DIRS = ["N", "S", "E", "W"];
  
  Snake.DIRDELTAS = {
    "N": new Coord(0, -1),
    "S": new Coord(0, 1),
    "E": new Coord(1, 0),
    "W": new Coord(-1, 0)
  };
  
  var snake = new Snake();
  console.log(snake.board.render(snake));
  snake.move();
  console.log(snake.board.render(snake));

}(this));

