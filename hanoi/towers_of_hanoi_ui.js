(function(root) {
  "use strict";
  var Hanoi = root.Hanoi = (root.Hanoi || {});
  
  var HanoiUI = Hanoi.HanoiUI = function(towers, game) {
    this.towers = towers;
    this.game = game;
  };
  
  HanoiUI.SIZES = {
    1: "small",
    2: "medium",
    3: "large"
  }
  
  HanoiUI.prototype.render = function() {
    this.game.towers.forEach(function(tower, towerId) {
      var $tower = $("ul")[towerId];
      
      tower.forEach(function(disc, discId) {
        var $disc = $($tower.children[2 - discId]);
        $disc.addClass(HanoiUI.SIZES[disc]);
      });
    });
  }
  
  $(document).ready(function() {
      
    var ui = new HanoiUI($('towers')[0], new Hanoi.Game());
    ui.render();
    
    $('ul').on("click", function(event) {
      if (!ui.highlightedTower) {
        var pile = $(this).data("id");
        var discId = ui.game.towers[pile].length - 1; 
        if(discId >= 0) {
          var disc = $(this).children()[2 - discId];
          $(disc).addClass("highlighted");
          ui.highlightedTower = [pile, disc];
        }
      } else {
        var toPile = $(this).data("id");
        var fromPile = ui.highlightedTower[0];
        var disc = ui.highlightedTower[1];
        var gameFromPile = ui.game.towers[fromPile]
        var gameToPile = ui.game.towers[toPile]
        
        
        var gameDiscClass = gameFromPile[gameFromPile.length - 1];
        var size = HanoiUI.SIZES[gameDiscClass]
        
        if (ui.game.move(ui.highlightedTower[0], toPile)) {
          $(disc).removeClass(size)
          $(disc).removeClass("highlighted");
          var targ = $($("ul")[toPile]).children()[2 - (gameToPile.length - 1)];
          $(targ).addClass(size);
          ui.highlightedTower = null;
        }
        
        if (ui.game.isWon()) {
          $("section").after("<h2>You win!</h2>");
        }
      }
    });
  });
}(this));