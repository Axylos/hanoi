(function(root) {
  "use strict";
  var TTT = root.TTT = (root.TTT || {});
  
  var TTTUI = TTT.TTTUI = function(gridUL, game) {
    this.gridUL = gridUL;
    this.game = game;
  };
  
  $(document).ready(function() {
    
    var ui = new TTTUI($('ul')[0], new TTT.Game());
    $("li").on("click", function(event) {
      console.log(this);
      var pos = $(this).data("pos");
      
      var parsedPos = [Math.floor(pos / 3), pos % 3];
      
      var player = ui.game.player;
      
      if (ui.game.move(parsedPos)) {
        // $(this).addClass(player);
      };
      
      if (ui.game.winner()) {
        $("ul").after($("<h2> " + ui.game.winner() + " won! </h2>"));
        $('body').on("dblclick", function() {
          location.reload();
        })
        
      };
      
      if (ui.game.tied()) {
        $("ul").after($("<h2> Cat's game! </h2>"));
      }
      
    });
  });
  
}(this));